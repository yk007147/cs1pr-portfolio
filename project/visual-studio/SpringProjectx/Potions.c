#include "common.h"

static void tick(void);
static void SpeedPtouch(Entity* other);
static void JumpPtouch(Entity* other);


void initJumpP(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/JumpP.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = JumpPtouch;

}

void initSpeedP(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/SpeedP.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = SpeedPtouch;



}

static void tick(void)
{
	self->value += 0.1;

	self->y += sin(self->value);
}

static void JumpPtouch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;
		player->jump = player->jump - 3;



	}
}

static void SpeedPtouch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		if (player->dx < 7)
		{
			player->PLAYER_MOVE_SPEED += 3;
		}

	}
}

