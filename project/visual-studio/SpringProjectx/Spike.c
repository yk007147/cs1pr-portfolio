#include "common.h"

static void Spiketouch(Entity* other);


void initSpike(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/Spike.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	//e->tick = tick;
	e->touch = Spiketouch;

}

static void Spiketouch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;
		player->health = player->health - 50;

		if (player->health == 0)
		{
			exit(1);
		}

	}
}